﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MyExchange.Authorization;

namespace MyExchange
{
    [DependsOn(
        typeof(MyExchangeCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MyExchangeApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<MyExchangeAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(MyExchangeApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}

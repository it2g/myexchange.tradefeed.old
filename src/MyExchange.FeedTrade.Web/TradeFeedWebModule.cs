﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MyExchange.FeedTrade.Web
{
    [DependsOn(
        typeof(MyExchangeWebCoreModule)
    )]
    public class TradeFeedWebModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TradeFeedWebModule).GetAssembly());
        }

        public override void PostInitialize()
        {

        }
    }
}

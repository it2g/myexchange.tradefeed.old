﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MyExchange.Configuration;
using MyExchange.Web;

namespace MyExchange.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ZeroDbContextFactory : IDesignTimeDbContextFactory<ZeroDbContext>
    {
        public ZeroDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ZeroDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ZeroDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MyExchangeConsts.ConnectionStringName));

            return new ZeroDbContext(builder.Options);
        }
    }
}

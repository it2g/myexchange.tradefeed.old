﻿using System.Collections.Generic;
using Abp.Configuration;
using Abp.Localization;

namespace MyExchange.FeedTrade.WebHost.AppSettings
{
    public class ApplicationSettingProvider : SettingProvider
    {
        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
            {
                new SettingDefinition("SmtpServerAddress",
                    "smtp.yandex.ru",
                    scopes: SettingScopes.Application | SettingScopes.Tenant,
                    displayName: new FixedLocalizableString("smpt server address")

                ),
                new SettingDefinition("SmtpServerPort",
                    "587",
                    scopes: SettingScopes.Application | SettingScopes.Tenant,
                    displayName: new FixedLocalizableString("smpt server port")
                ),
            };
        }
    }
}

﻿using Microsoft.AspNetCore.Antiforgery;
using MyExchange.Controllers;

namespace MyExchange.FeedTrade.WebHost.Controllers
{
    public class AntiForgeryController : MyExchangeControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}

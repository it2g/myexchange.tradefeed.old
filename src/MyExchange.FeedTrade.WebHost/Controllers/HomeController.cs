﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MyExchange.Controllers;

namespace MyExchange.FeedTrade.WebHost.Controllers
{
    public class HomeController : MyExchangeControllerBase
    {
        public HomeController()
        {
        }

        public IActionResult Index() => Redirect("/swagger");
    }
}

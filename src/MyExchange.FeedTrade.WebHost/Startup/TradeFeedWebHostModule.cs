﻿using System.Linq;
using Abp.BackgroundJobs;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;
using MyExchange.FeedTrade.Web;
using MyExchange.ExchangeExmo;
using MyExchange.ExchangeHuobiPro;
using MyExchange.BitcoinExchange;

namespace MyExchange.FeedTrade.WebHost
{
    [DependsOn(typeof(AbpHangfireAspNetCoreModule),
               typeof(TradeFeedModule),
               typeof(TradeFeedWebModule),
               typeof(MyExchangeMainModule),
               typeof(ExmoExchangeModule),
               typeof(HuobiProExchangeModule),
               typeof(BitcoinExchangeModule)
    )]
    public class TradeFeedWebHostModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.BackgroundJobs.UseHangfire();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TradeFeedWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
        }
    }
}

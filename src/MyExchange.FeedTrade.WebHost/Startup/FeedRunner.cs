﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Dependency;
using JetBrains.Annotations;
using Microsoft.Extensions.Hosting;
using MyExchange.Api.Constants;
using MyExchange.Api.Dto.PairInfoOnExchange;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;
using MyExchange.FeedTrade.Api;
using MyExchange.FeedTrade.Api.Dal;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade.WebHost
{
    /// <summary>
    /// Размещенная служба, запускающая фоновый процесс сбора фида
    /// </summary>
    public class FeedRunner : IHostedService
    {
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IPairInfoOnExchangeCrudAppService _pairInfoOnExchangeCrudAppService;
        private readonly ITradeFeedContextRepository _feedTacticContextRepository;
        private readonly ITradeFeedBuilder _feedTacticBuilder;
        private readonly ITradeFeedManager _tradeFeedTacticManager;
        private readonly object _locker = new object();

        public FeedRunner([NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IBackgroundJobManager backgroundJobManager,
            [NotNull] IPairInfoOnExchangeCrudAppService pairInfoOnExchangeCrudAppService,
            [NotNull] ITradeFeedContextRepository feedTacticContextRepository,
            [NotNull] ITradeFeedBuilder feedTacticBuilder,
            [NotNull] ITradeFeedManager tradeFeedTacticManager)
        {
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _backgroundJobManager = backgroundJobManager ?? throw new ArgumentNullException(nameof(backgroundJobManager));
            _pairInfoOnExchangeCrudAppService = pairInfoOnExchangeCrudAppService ?? throw new ArgumentNullException(nameof(pairInfoOnExchangeCrudAppService));
            _feedTacticContextRepository = feedTacticContextRepository ?? throw new ArgumentNullException(nameof(feedTacticContextRepository));
            _feedTacticBuilder = feedTacticBuilder ?? throw new ArgumentNullException(nameof(feedTacticBuilder));
            _tradeFeedTacticManager = tradeFeedTacticManager ?? throw new ArgumentNullException(nameof(tradeFeedTacticManager));
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
           return  new TaskFactory().StartNew(async () =>
            {
                var exchanges = _exchangeProvider.GetAll();

                foreach (var exchange in exchanges)
                {
                    if (exchange.Id == "ExmoExchange")
                    {
                        var pairInfoOnExchangeList = await _pairInfoOnExchangeCrudAppService.GetAll(
                            new PairInfoOnExchangePagedResultRequestDto { ExchangeId = exchange.Id });

                        RunFeed(exchange.Id, pairInfoOnExchangeList.Items.Select(p=> p.PairId).JoinAsString(";"));
                    }
                }
            },
               cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            //TODO Следует продумать и реализовать логику останова процесса 
            return new TaskFactory().StartNew(() => { }, cancellationToken);
        }

        void RunFeed(string exchangeId, string pairIds)
        {
            TradeFeedContext feedContext;
            lock (_locker)
            {
                feedContext = _feedTacticContextRepository
                    .FirstOrDefault(x => x.ExchangeId == exchangeId
                                         && x.PairIds == pairIds
                                         && x.Status == BotStatuses.Work);
            }

            if (feedContext == null)
            {
                feedContext = new TradeFeedContext
                {
                    Pause = 10000,
                    PauseDelta = 200,
                    PairIds = pairIds,
                    ExchangeId = exchangeId
                };
            }
            else
            {
                Debug.WriteLine("Поднят из БД контекст мониторинга");
            }

            var botTactic = _feedTacticBuilder.Create(feedContext);

            lock (_locker)
            {
                _tradeFeedTacticManager.Start(botTactic);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MyExchange.FeedTrade.WebHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var builder = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder()
                .UseKestrel()
                .UseUrls("http://0.0.0.0:22024") //https://github.com/dotnet/core/issues/948  http://take.ms/VaudN
                .UseStartup<Startup>()
                .ConfigureServices(services =>
                {
                    //Запускается фоновый процесс получения фида
                    services.AddHostedService<FeedRunner>();
                });

            return builder;
        }
    }
}

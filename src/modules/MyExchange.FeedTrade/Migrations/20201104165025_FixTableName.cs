﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyExchange.FeedTrade.Migrations
{
    public partial class FixTableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "feed");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "trade_feed_context",
                schema: "feed",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    creation_time = table.Column<DateTime>(nullable: false),
                    status_id = table.Column<int>(nullable: false),
                    start_time = table.Column<DateTime>(nullable: true),
                    stop_time = table.Column<DateTime>(nullable: true),
                    pair_ids = table.Column<string>(nullable: false),
                    exchange_id = table.Column<string>(nullable: false),
                    pause = table.Column<int>(nullable: false),
                    pause_delta = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_trade_feed_context", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "trade_feed_context",
                schema: "feed");
        }
    }
}

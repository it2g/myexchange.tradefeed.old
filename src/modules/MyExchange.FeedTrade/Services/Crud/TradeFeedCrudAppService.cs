﻿using System;
using Abp.Domain.Repositories;
using MyExchange.FeedTrade.Api.Dto;
using MyExchange.FeedTrade.Api.Entity;
using MyExchange.FeedTrade.Api.Services.Crud;

namespace MyExchange.FeedTrade.Services.Crud
{
    public class TradeFeedCrudAppService :
        TradeFeedCrudServiceBase<TradeFeedContext, TradeFeedContextDto, TradeFeedContextPagedResultRequestDto, Guid>,
        ITradeFeedCrudAppService
    {
        public TradeFeedCrudAppService(IRepository<TradeFeedContext, Guid> repository) : base(repository)
        {

        }
    }

}

﻿using MyExchange.FeedTrade.Api.Dto;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade
{
    public class TradeFeedContextAutoMapperProfile : AutoMapper.Profile
    {
        public TradeFeedContextAutoMapperProfile()
        {
            CreateMap<TradeFeedContextDto, TradeFeedContext>().ReverseMap();
            CreateMap<CreateTradeFeedContextDto, TradeFeedContextDto>();
            CreateMap<UpdateTradeFeedContextDto, TradeFeedContextDto>();
        }
    }
}

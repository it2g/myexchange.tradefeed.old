﻿using Abp.AutoMapper;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using MyExchange.Api;
using MyExchange.Base;
using MyExchange.EntityFrameworkCore;
using MyExchange.FeedTrade.Dal;

namespace MyExchange.FeedTrade
{
    [DependsOn(typeof(MyExchangeBaseModule),
               typeof(MyExchangeEntityFrameworkModule),
               typeof(AbpAutoMapperModule))]
    public class TradeFeedModule : AbpModule
    {
        public bool SkipDbContextRegistration { get; set; }

        /// <summary>
        /// Предварительное подключение конвенций регистрации для контейнера,
        /// для автоматической регистрации контекстов данных, конфигураций и 
        /// компонентов системы (репозиториев и сервисов).
        /// </summary>

        public override void PreInitialize()
        {
            //IocManager.AddConventionalRegistrar(new ConfigurationConventionalRegistrar());
            //IocManager.AddConventionalRegistrar(new ComponentConventionalRegistrar());
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<TradeFeedDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        TradeFeedDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        TradeFeedDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
            //Настройка конфигурации Automapper
            Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
                config.AddProfiles(typeof(TradeFeedModule).Assembly));

            // включение поддержки внедрения коллекции зависимостей.
            // Эта настройка позволяет контейнеру внедрять зависимости в свойства вида T[], IList<T>, IEnumerable<T>, ICollection<T>,
            // пытаясь подставить в них коллекцию со всеми зайденными реализациями типа T. 
            // Если ни одной зависимости типа T нет, контейнер внедрит пустую коллекцию
            IocManager.IocContainer.Kernel.Resolver.AddSubResolver(new CollectionResolver(IocManager.IocContainer.Kernel, true));
        }

        /// <summary>
        /// Регистрация компонентов текущей сборки и сборки модели справочников в контейнере
        /// </summary>
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TradeFeedModule).Assembly);
        }

        /// <summary>
        /// Выполнение настроечной логики после завершения сборки контейнера
        /// </summary>
        public override void PostInitialize()
        {
        }
    }
}

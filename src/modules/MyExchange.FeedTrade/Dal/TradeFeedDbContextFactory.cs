﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MyExchange.Configuration;
using MyExchange.Web;

namespace MyExchange.FeedTrade.Dal
{
    public class TradeFeedDbContextFactory : IDesignTimeDbContextFactory<TradeFeedDbContext>
    {
        public TradeFeedDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TradeFeedDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(),
                environmentName: Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));

            TradeFeedDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MyExchangeConsts.ConnectionStringName));

            return new TradeFeedDbContext(builder.Options);
        }
    }
}

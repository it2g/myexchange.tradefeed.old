﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyExchange.Api.Dal.EfConfigurations;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade.Dal.EfConfigurations
{
    public class TradeFeedContextMap : BotTacticsContextBaseMap<TradeFeedContext>
    {
        public override void Configure(EntityTypeBuilder<TradeFeedContext> builder)
        {
            base.Configure(builder);

            builder.Property(u => u.PairIds).IsRequired().HasColumnName("pair_ids");
            builder.Property(u => u.Pause).IsRequired().HasColumnName("pause");
            builder.Property(u => u.PauseDelta).IsRequired().HasColumnName("pause_delta");
            builder.Property(x => x.ExchangeId).IsRequired().HasColumnName("exchange_id");

            builder.ToTable("trade_feed_context", "feed");
        }
    }
}

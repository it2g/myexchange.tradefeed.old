﻿using Abp.EntityFrameworkCore;
using Castle.Core;
using Microsoft.EntityFrameworkCore;
using MyExchange.Api.Entity;
using MyExchange.Api.Exchange;
using MyExchange.FeedTrade.Api.Entity;
using MyExchange.FeedTrade.Dal.EfConfigurations;

namespace MyExchange.FeedTrade.Dal
{
    public class TradeFeedDbContext : AbpDbContext
    {
        public virtual DbSet<TradeFeedContext> MonitoringBotTacticContexts { get; set; }

        public TradeFeedDbContext(DbContextOptions<TradeFeedDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ApplicationLanguageText>()
            //    .Property(p => p.Value)
            //    .HasMaxLength(10485759); // any integer that is smaller than 10485760
            base.OnModelCreating(modelBuilder);
            //https://www.npgsql.org/efcore/value-generation.html - для автогенерации Id типа Guid
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.ApplyConfiguration(new TradeFeedContextMap());

            modelBuilder.Ignore < Pair> ();
            modelBuilder.Ignore<Currency>();
            modelBuilder.Ignore<Exchange>();
        }
    }
}

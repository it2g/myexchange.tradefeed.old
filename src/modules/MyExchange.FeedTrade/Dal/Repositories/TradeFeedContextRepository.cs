﻿using System;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Repositories;
using MyExchange.FeedTrade.Api.Dal;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade.Dal.Repositories
{
    public class TradeFeedContextRepository :
        EfCoreRepositoryBase<TradeFeedDbContext, TradeFeedContext, Guid>,
        ITradeFeedContextRepository
    {
        public TradeFeedContextRepository(IDbContextProvider<TradeFeedDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public override TradeFeedContext Insert(TradeFeedContext entity)
        {
            entity = base.Insert(entity);

            return entity;
        }
    }
}

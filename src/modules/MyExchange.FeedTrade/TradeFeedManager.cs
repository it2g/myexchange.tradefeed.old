﻿using Abp.BackgroundJobs;
using MyExchange.Api.BotTactic;
using MyExchange.Api.Exchange;
using MyExchange.Base.Services;
using MyExchange.FeedTrade.Api;
using MyExchange.FeedTrade.Api.Dal;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade
{
    /// <summary>
    /// Менеджер, котрый управляет ботами мониторинга
    /// </summary>
    public class TradeFeedManager : TradingTacticManagerBase<ITradeFeed, TradeFeedContext>
        , ITradeFeedManager
    {
        public TradeFeedManager(IBotTacticProvider botTacticProvider,
            IExchangeProvider exchangeProvider,
            IBackgroundJobManager backgroundJobManager,
            ITradeFeedContextRepository contextRepository)
            : base(botTacticProvider, exchangeProvider, backgroundJobManager, contextRepository)
        {
        }
    }
}

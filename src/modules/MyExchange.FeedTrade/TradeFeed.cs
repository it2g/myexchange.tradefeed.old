﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Uow;
using AutoMapper;
using JetBrains.Annotations;
using MyExchange.Api.Dto.Pair;
using MyExchange.Api.Dto.Trade;
using MyExchange.Api.Exchange;
using MyExchange.Api.Services.Crud;
using MyExchange.Base.BotTactic;
using MyExchange.FeedTrade.Api;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade
{
    /// <summary>
    /// Мониторинг курсов валютных пар
    /// </summary>
    public class TradeFeed : BotTacticBase<TradeFeedContext>, ITradeFeed
    {
        private readonly ITradeCrudAppService _tradeCrudService;
        private readonly IExchangeProvider _exchangeProvider;
        private readonly IPairCrudAppService _pairCrudAppService;
        private static readonly object _lockerPairRead = new object();
        private static readonly object _lockerTradeSave = new object();
        private static readonly object _lockerExchangeTradeLoad = new object();


        public TradeFeed(
            [NotNull] ITradeCrudAppService tradeCrudService,
            [NotNull] IExchangeProvider exchangeProvider,
            [NotNull] IPairCrudAppService pairCrudAppService)
        {
            _tradeCrudService = tradeCrudService ?? throw new ArgumentNullException(nameof(tradeCrudService));
            _exchangeProvider = exchangeProvider ?? throw new ArgumentNullException(nameof(exchangeProvider));
            _pairCrudAppService = pairCrudAppService ?? throw new ArgumentNullException(nameof(pairCrudAppService));
        }
        public override string Name { get; } = "Мониторинг сделок по валютной паре";

        public override Action Execute => Monitoring;

        public override void Dispose() { }

        /// <summary>
        /// Собирает информацию о курсах валютных пар и сохраняет ее в БД для последующей аналитик
        /// </summary>
        [UnitOfWork(IsDisabled = false)]
        private async void Monitoring()
        {
            //Сделки загруженные на предыдущем запросе
            IDictionary<PairDto, IEnumerable<TradeDto>> previousTrades = new Dictionary<PairDto, IEnumerable<TradeDto>>();

            var exchange = _exchangeProvider.Get(Context.ExchangeId);


            IList<PairDto> pairs = new List<PairDto>();

            var pairIdList = Context.PairIds.Split(";");

            foreach (var pairId in pairIdList)
            {
                //TODO организовать агрегированный запрос на получение всех пар сразу
                pairs.Add(await _pairCrudAppService.Get(pairId));
            }

            ///В цикле с установленной задержкой делать запросы на биржу по совершаемым
            /// сделкам отслеживаемой валютной пары.
            do
            {
                //lock (_lockerExchangeTradeLoad)
                //{
                Debug.WriteLine($"Поток {Thread.CurrentThread.ManagedThreadId} начал читать  сделки с биржи");
                var trades = await exchange.ExchangeDriver.TradesAsync(pairs);
                Debug.WriteLine($"Поток {Thread.CurrentThread.ManagedThreadId} прочитал {trades.Count}  сделок");
                //}

                if (trades.Count == 0) break;

                ///Оставляем только новые сделки, т.к. в последнем запросе естественно
                /// будут сделки, котрые были уже получены в предпоследнем запросе
                var lastNewTrades = GetOnlyNewTrades(trades, previousTrades);

                lock (_lockerTradeSave)
                {
                    var tradesDto = _tradeCrudService
                        .AddList(lastNewTrades.Select(e => ObjectMapper.Map<TradeDto>(e)).ToList()).Result;
                }

                var countNewTrades = lastNewTrades.Count;

                //Изменение задержки перед последующей загрузкой на основе анализа
                //повторно скаченных данных
               //ChangePause(countNewTrades, trades.Count());
#if DEBUG
                Debug.WriteLine($"Количество новых данных: {countNewTrades} Задержка: {Context.Pause} милисек");
#endif
                //сохраняем последние сделки, чтобы сравнить с ними загружаемые сделки на 
                //следующем шаге
                previousTrades = trades;

                await Task.Delay(Context.Pause);

                //Ведется контроль за выставленным состоянием бота и в случае необходимости
                //прекращает мониторинг
                //https://docs.microsoft.com/ru-ru/dotnet/standard/parallel-programming/task-cancellation
                if (Context.CancellationTokenSource.IsCancellationRequested)
                {
                    ///TODO Предварительные операции по завершению работы мониторинга

                    Context.CancellationTokenSource.Token.ThrowIfCancellationRequested();
                }

            } while (true);

        }

        /// <summary>
        /// Вносит коррективы в величину паузы между двумя загрузками данных
        /// в зависимости от того, насколько быстро меняются данные.
        /// Настоящая реализация возможно несколько грубо отражает возможную динамику
        /// Этот  алгоритм можно уточнять и делать более диференцированый и более динамичный
        /// </summary>
        /// <param name="countNewTrades">количество новых сделок</param>
        /// <param name="countTrades"></param>
        private void ChangePause(int countNewTrades, int countTrades)
        {
            if (countTrades == 0) return;

            if (countNewTrades / countTrades < 0.4)
            {
                //Увеличиваем задержку в 2 раза
                Context.Pause *= 2;
            }
            else if (countNewTrades / countTrades < 0.6)
            {
                //Увеличиваем задержку на дельту
                Context.Pause += Context.PauseDelta;
            }
            else if (countNewTrades / countTrades > 0.8)//80% всех загруженных сделок - это новые сделки
            {
                //Уменьшаем задержку на дельту
                Context.Pause -= Context.PauseDelta;
            }
            else if (countNewTrades / countTrades > 0.9)//90% всех загруженных сделок - это новые сделки
            {
                //Уменьшаем задержку в 2 раза
                Context.Pause /= 2;
            }
        }

        /// <summary>
        /// Выбирает и возвращает только новые сделки по отношению к выборке 
        /// на предыдущей итеррации. Нет необходимости коммитить в БД одни и те же сделки
        /// </summary>
        /// <param name="trades"></param>
        /// <param name="previousTrades"></param>
        /// <returns></returns>
        private IList<TradeDto> GetOnlyNewTrades(IDictionary<PairDto, IEnumerable<TradeDto>> trades, IDictionary<PairDto, IEnumerable<TradeDto>> previousTrades)
        {
            var allTrades = trades.SelectMany(x => x.Value);

            var allPreviousTrades = previousTrades.SelectMany(x => x.Value);

            var newTrades = allTrades.Except(allPreviousTrades, new TradeComparer()).ToList();

            return newTrades;
        }

        

        /// <summary>
        /// Сравниватель двух сделок, загруженных из биржи
        /// </summary>
        class TradeComparer : IEqualityComparer<TradeDto>
        {
            public bool Equals(TradeDto x, TradeDto y)
            {
                return x.TradeId == y.TradeId;
            }

            public int GetHashCode(TradeDto obj)
            {
                return obj.TradeId.GetHashCode();
            }
        }
    }
}

﻿using Abp.Application.Services;
using Abp.Dependency;
using MyExchange.FeedTrade.Api;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade
{
    public class TradeFeedBuilder : ApplicationService, ITradeFeedBuilder
    {
        public ITradeFeed Create(TradeFeedContext context)
        {
            var botTactic = IocManager.Instance.Resolve<ITradeFeed>();

            botTactic.Context = context;

            return botTactic;
        }
    }
}

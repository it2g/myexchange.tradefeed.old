﻿using MyExchange.Api.BotTactic;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade.Api
{
    public interface ITradeFeedManager : IBotManager<ITradeFeed, TradeFeedContext>
    {
    }
}

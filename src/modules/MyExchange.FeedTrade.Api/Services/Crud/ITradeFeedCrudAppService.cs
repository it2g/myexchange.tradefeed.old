﻿using System;
using MyExchange.Api.Services.Crud;
using MyExchange.FeedTrade.Api.Dto;

namespace MyExchange.FeedTrade.Api.Services.Crud
{
    public interface ITradeFeedCrudAppService :
        IExhchangeCrudAppServices<TradeFeedContextDto, Guid, TradeFeedContextPagedResultRequestDto,
            TradeFeedContextDto, TradeFeedContextDto, TradeFeedContextDto, TradeFeedContextDto>
    {
    }
}

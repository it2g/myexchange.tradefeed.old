﻿namespace MyExchange.FeedTrade.Api.Dto
{
    public class UpdateTradeFeedContextDto
    {
        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы если появляется много
        /// повтороно загружаемых сделок, чтобы сократить количество избыточно-лишних запросов
        public int PauseDelta { get; set; } = 100;
    }
}

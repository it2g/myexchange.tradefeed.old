﻿using System;
using MyExchange.Api.Dto._DtoBase;

namespace MyExchange.FeedTrade.Api.Dto
{
    public class TradeFeedContextDto : BotTacticContextBaseDto
    {
        public Guid PairId { get; set; }

        public string ExchangeId { get; set; }

        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 3000;

        /// Временное приращение/подрезание паузы если появляется много
        /// повтороно загружаемых сделок, чтобы сократить количество избыточно-лишних запросов
        public int PauseDelta { get; set; } = 100;
    }
}

﻿using Abp.Application.Services;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade.Api
{
    public interface ITradeFeedBuilder : IApplicationService
    {
        ITradeFeed Create(TradeFeedContext context);
    }
}

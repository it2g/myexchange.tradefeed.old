﻿using MyExchange.Api.BotTactic;

namespace MyExchange.FeedTrade.Api.Entity
{
    public class TradeFeedContext : BotTacticContextBase
    {
        /// <summary>
        /// Список идентификаторов загружаемых пар разделенный точкой с запятой,
        /// </summary>
        public string PairIds { get; set; }

        public string ExchangeId { get; set; }

        ///Задержка между запросами к бирже 
        public int Pause { get; set; } = 10000;

        /// Временное приращение/подрезание паузы если появляется много
        /// повтороно загружаемых сделок, чтобы сократить количество избыточно-лишних запросов
        public int PauseDelta { get; set; } = 100;
    }
}

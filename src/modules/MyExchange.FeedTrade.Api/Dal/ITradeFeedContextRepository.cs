﻿using System;
using Abp.Domain.Repositories;
using MyExchange.FeedTrade.Api.Entity;

namespace MyExchange.FeedTrade.Api.Dal
{
    public interface ITradeFeedContextRepository : IRepository<TradeFeedContext, Guid>
    {
    }
}
